const express = require('express');
const ctrl = require('../../controllers/users');
const { validateBody, authenticate } = require('../../middlewares');
const { schemas } = require('../../models/user');
const router = express.Router();

router.post('/signup', validateBody(schemas.signupSchema), ctrl.signup);
router.post('/signin', validateBody(schemas.signinSchema), ctrl.signin);
router.get('/current', authenticate, ctrl.currentUser);
router.post('/signout', authenticate, ctrl.signout);

module.exports = router;
