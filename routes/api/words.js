const express = require('express');
const ctrl = require('../../controllers/words');
const { validateBody, authenticate, isValidId } = require('../../middlewares');
const { schemas } = require('../../models/word');
const router = express.Router();

router.get('/categories', authenticate, ctrl.getCategories);
router.post('/create', authenticate, validateBody(schemas.createWord), ctrl.createWord);
router.post('/add/:id', authenticate, isValidId, ctrl.addWord);
router.patch('/edit/:id', authenticate, isValidId, validateBody(schemas.editWord), ctrl.editWord);
router.get('/all', authenticate, ctrl.getAllWords);
router.get('/own', authenticate, ctrl.getOwnWords);
router.delete('/delete/:id', authenticate, isValidId, ctrl.removeWord);
router.get('/statistics', authenticate, ctrl.getStatistics);
router.get('/tasks', authenticate, ctrl.getTasks);
router.post('/answers', authenticate, validateBody(schemas.postAnswers), ctrl.postAnswers);

module.exports = router;
