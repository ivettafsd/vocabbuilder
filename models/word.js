const { handleMangooseError, HttpError } = require('../helpers');
const { Schema, model } = require('mongoose');
const Joi = require('joi');

const enRegexp = /\b[A-Za-z'-]+(?:\s+[A-Za-z'-]+)*\b/;
const uaRegexp = /^(?![A-Za-z])[А-ЯІЄЇҐґа-яієїʼ\s]+$/u;

const wordSchema = Schema(
  {
    en: {
      type: String,
      match: enRegexp,
      required: true,
    },
    ua: {
      type: String,
      match: uaRegexp,
      required: true,
    },
    category: {
      type: String,
      enum: ['verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase'],
      required: true,
    },
    isIrregular: {
      type: Boolean,
      required: function () {
        return this.category === 'verb';
      },
    },
    isEnSpelling: {
      type: Boolean,
      required: true,
      default: false,
    },
    isUaSpelling: {
      type: Boolean,
      required: true,
      default: false,
    },
    owner: {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
  },
  { versionKey: false, timestamps: true },
);

wordSchema.post('save', handleMangooseError);

const createWord = Joi.object({
  en: Joi.string()
    .required()
    .empty(false)
    .when('isIrregular', {
      is: true,
      then: Joi.string()
        .pattern(/^\w+-\w+-\w+$/)
        .required()
        .messages({
          'string.pattern.base': 'The en field must have the format "verb I form-verb II form-verb III form" for the "verb" category and irregular is true',
        }),
      otherwise: Joi.string().pattern(enRegexp).required().messages({
        'string.pattern.base': 'The en field can include only English letters and usual for English words and sentences symbols',
      }),
    }),
  ua: Joi.string().required().empty(false).pattern(uaRegexp).messages({
    'string.base': 'The ua field must be a string',
    'any.required': 'The ua field is required',
    'string.empty': 'The ua field must not be empty',
    'string.pattern.base': 'The ua field can include only Ukrainian letters and usual for Ukrainian words and sentences symbols',
  }),
  category: Joi.string()
    .valid('verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase')
    .required()
    .messages({
      'string.base': 'The category field must be a string',
      'any.only':
        "Invalid category. Allowed values are 'verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase'",
      'any.required': 'The category field is required',
    }),
  isIrregular: Joi.boolean().when('category', {
    is: Joi.string().valid('verb'),
    then: Joi.boolean().required().messages({
      'any.required': "The isIrregular field is required for the 'verb' category",
      'boolean.base': 'The isIrregular field must be a boolean',
    }),
    otherwise: Joi.boolean().forbidden(),
  }),
});

const editWord = Joi.object({
  en: Joi.string().when('isIrregular', {
    is: true,
    then: Joi.string()
      .pattern(/^\w+-\w+-\w+$/)
      .required()
      .messages({
        'string.pattern.base': 'The en field must have the format "verb I form-verb II form-verb III form" for the "verb" category and irregular is true',
      }),
    otherwise: Joi.string().pattern(enRegexp).messages({
      'string.pattern.base': 'The en field can include only English letters and usual for English words and sentences symbols',
    }),
  }),
  ua: Joi.string().pattern(uaRegexp).messages({
    'string.base': 'The ua field must be a string',
    'string.pattern.base': 'The ua field can include only Ukrainian letters and usual for Ukrainian words and sentences symbols',
  }),
  category: Joi.string()
    .valid('verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase')
    .messages({
      'string.base': 'The category field must be a string',
      'any.only':
        "Invalid category. Allowed values are 'verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase'",
      'any.required': 'The category field is required',
    }),
  isIrregular: Joi.boolean().when('category', {
    is: Joi.string().valid('verb'),
    then: Joi.boolean().required().messages({
      'any.required': "The isIrregular field is required for the 'verb' category",
      'boolean.base': 'The isIrregular field must be a boolean',
    }),
    otherwise: Joi.boolean().forbidden(),
  }),
});

const answerSchema = Joi.object({
  _id: Joi.string().required().messages({
    'string.base': 'The _id field must be a string.',
    'any.required': 'The _id field is required.',
  }),
  en: Joi.string().required().allow(null).pattern(enRegexp).messages({
    'string.base': 'The en field must be a string.',
    'any.required': 'The en field is required.',
    'string.pattern.base': 'The en field can include only English letters and usual for English words and sentences symbols',
  }),
  ua: Joi.string().required().allow(null).pattern(uaRegexp).messages({
    'string.base': 'The ua field must be a string.',
    'any.required': 'The ua field is required.',
    'string.pattern.base': 'The ua field can include only Ukrainian letters and usual for Ukrainian words and sentences symbols',
  }),
  task: Joi.string().valid('en', 'ua').messages({
    'string.base': 'The task field must be a string',
    'any.only': "Invalid task. Allowed values are 'en', 'ua'",
    'any.required': 'The task field is required',
  }),
});

const postAnswers = Joi.array().items(answerSchema);

const Word = model('word', wordSchema);
const schemas = { createWord, editWord, postAnswers };

module.exports = { Word, schemas };
