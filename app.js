const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const usersRouter = require('./routes/api/auth');
const wordsRouter = require('./routes/api/words');
const app = express();

const formatsLogger = app.get('env') === 'development' ? 'dev' : 'short';

app.use(logger(formatsLogger));
app.use(cors());
app.use(express.json());

const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/api/users', usersRouter);
app.use('/api/words', wordsRouter);

app.use((req, res) => {
  res.status(404).json({ message: 'Service not found' });
});

app.use((err, req, res, next) => {
  const { status = 500, message = 'Server error' } = err;
  res.status(status).json({ message });
});

module.exports = app;
