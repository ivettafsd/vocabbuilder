const getCategories = async (req, res) => {
    const result = ['verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase'];
    res.json(result);
};

module.exports = getCategories;