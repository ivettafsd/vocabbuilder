const { Word } = require('../../models/word');

const getStatistics = async (req, res) => {
  const { _id } = req.user;

  const query = {
    owner: _id,
    $or: [
      { isEnSpelling: false, isUaSpelling: true },
      { isEnSpelling: true, isUaSpelling: false },
      { isEnSpelling: false, isUaSpelling: false },
    ],
  };
  const result = await Word.aggregate([
    { $match: query },
    {
      $facet: {
        totalCount: [{ $count: 'totalItems' }]
      },
    },
    {
      $project: {
        totalCount: {
          $ifNull: [{ $arrayElemAt: ['$totalCount.totalItems', 0] }, 0],
        }
      },
    },
  ]);

  res.json(result[0]);
};

module.exports = getStatistics;
