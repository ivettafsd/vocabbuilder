const { Word } = require('../../models/word');
const { HttpError } = require('../../helpers');

const createWord = async (req, res) => {
  const { _id } = req.user;
  const { en: enWord, ua: uaWord } = req.body;

  const foundWord = await Word.findOne({ en: enWord, ua: uaWord, owner: _id });
  if (foundWord) {
    throw HttpError(409, 'Such a word exists');
  }

  const newWord = {
    ...req.body,
    owner: _id,
  };
  const createdWord = await Word.create(newWord);

  const result = await Word.findById(createdWord._id).select('-createdAt -updatedAt');

  const { isEnSpelling, isUaSpelling, ...handledResult } = result.toObject();

  let progress;
  if (isEnSpelling && isUaSpelling) {
    progress = 100;
  } else if (isEnSpelling || isUaSpelling) {
    progress = 50;
  } else {
    progress = 0;
  }

  res.json({ ...handledResult, progress });
};

module.exports = createWord;
