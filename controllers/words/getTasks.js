const { Word } = require('../../models/word');

const getTasks = async (req, res) => {
  const { _id } = req.user;

  const query = {
    owner: _id,
    $or: [
      { isEnSpelling: false, isUaSpelling: true },
      { isEnSpelling: true, isUaSpelling: false },
      { isEnSpelling: false, isUaSpelling: false },
    ],
  };
  const result = await Word.aggregate([
    { $match: query },
    {
      $facet: {
        totalCount: [{ $count: 'totalItems' }],
        enSpellingFalse: [{ $match: { isEnSpelling: false } }, { $project: { ua: '$ua', task: 'en', _id: 1 } }],
        uaSpellingFalse: [{ $match: { isUaSpelling: false } }, { $project: { en: '$en', task: 'ua', _id: 1 } }],
      },
    },
    {
      $project: {
         tasks: { $concatArrays: ['$enSpellingFalse', '$uaSpellingFalse'] },
      },
    },
  ]);

  res.json(result[0]);
};

module.exports = getTasks;
