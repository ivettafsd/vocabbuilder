const { Word } = require('../../models/word');

const getAllWords = async (req, res) => {
  const { _id } = req.user;
  const { keyword, category, isIrregular, page = 1, limit = 7 } = req.query;

  const skip = (Number(page) - 1) * Number(limit);
  const query = { owner: { $ne: _id } };
  if (keyword) {
    query.$or = [{ en: { $regex: keyword, $options: 'i' } }, { ua: { $regex: keyword, $options: 'i' } }];
  }
  if (category) {
    query.category = category;
    if (category === 'verb' && isIrregular) {
      query.isIrregular = JSON.parse(isIrregular);
    }
  } else {
    if (isIrregular) {
      query.category = 'verb';
      query.isIrregular = JSON.parse(isIrregular);
    }
  }

  const result = await Word.aggregate([
    { $match: query },
    {
      $group: {
        _id: '$en',
        doc: { $first: '$$ROOT' },
      },
    },
    {
      $replaceRoot: { newRoot: '$doc' },
    },
    { $sort: { updatedAt: -1 } },
    {
      $facet: {
        metadata: [
          { $count: 'totalCount' },
          { $addFields: { page: Number(page), perPage: Number(limit) } },
          { $addFields: { totalPages: { $ceil: { $divide: ['$totalCount', '$perPage'] } } } },
        ],
        results: [{ $skip: skip }, { $limit: Number(limit) }, { $unset: ['createdAt', 'updatedAt', 'isEnSpelling', 'isUaSpelling', 'owner'] }],
      },
    },
    {
      $project: {
        totalPages: { $arrayElemAt: ['$metadata.totalPages', 0] },
        page: { $arrayElemAt: ['$metadata.page', 0] },
        perPage: { $arrayElemAt: ['$metadata.perPage', 0] },
        results: 1,
      },
    },
  ]);

  res.json(result[0]);
};

module.exports = getAllWords;
