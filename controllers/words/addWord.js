const { Word } = require('../../models/word');
const { HttpError } = require('../../helpers');

const addWord = async (req, res) => {
  const { _id } = req.user;
  const { id: idWord } = req.params;

  const foundNewWord = await Word.findOne({ _id: idWord, owner: { $ne: _id } }).select('-createdAt -updatedAt');

  if (!foundNewWord) {
    throw HttpError(404, 'This word not found');
  }

  const foundOwnWord = await Word.findOne({ en: foundNewWord.en, ua: foundNewWord.ua, owner: _id });
  if (foundOwnWord) {
    throw HttpError(409, 'Such a word exists');
  }

  const newWord = {
    en: foundNewWord.en,
    ua: foundNewWord.ua,
    category: foundNewWord.category,
    ...(foundNewWord.category === 'verb' && { isIrregular: foundNewWord.isIrregular }),
    owner: _id,
  };
  const createdWord = await Word.create(newWord);

  const result = await Word.findById(createdWord._id).select('-createdAt -updatedAt');

  const { isEnSpelling, isUaSpelling, ...handledResult } = result.toObject();

  let progress;
  if (isEnSpelling && isUaSpelling) {
    progress = 100;
  } else if (isEnSpelling || isUaSpelling) {
    progress = 50;
  } else {
    progress = 0;
  }
  res.json({ ...handledResult, progress });
};

module.exports = addWord;
