const { Word } = require('../../models/word');

const postAnswers = async (req, res) => {
  const { _id } = req.user;
  const answers = req.body;

  const updatePromises = answers.map(async (answer) => {
    const query = { owner: _id, _id: answer._id, en: answer.en, ua: answer.ua };
    const update = answer.task === 'en' ? { isEnSpelling: true } : { isUaSpelling: true };
    const updatedWord = await Word.findOneAndUpdate(query, update, { new: true });
    return {
      ...answer,
      isDone: !!updatedWord,
    };
  });

  const updatedResults = await Promise.all(updatePromises);

  res.json(updatedResults);
};

module.exports = postAnswers;
