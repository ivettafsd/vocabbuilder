const { Word } = require('../../models/word');
const { HttpError } = require('../../helpers');

const removeWord = async (req, res) => {
  const { _id } = req.user;
  const { id: wordId } = req.params;

  const foundWord = await Word.findOneAndRemove({ _id: wordId, owner: _id });
  if (!foundWord) {
    throw HttpError(404, 'This word not found');
  }

  const result = {
    message: 'This word was deleted',
    id: wordId,
  };
  res.json(result);
};

module.exports = removeWord;
