const { HttpError } = require('../../helpers');
const { Word } = require('../../models/word');

const editWord = async (req, res) => {
  const { _id: ownerId } = req.user;
  const { id: wordId } = req.params;
  const foundWord = await Word.findOne({ _id: wordId });

  if (!foundWord) {
    throw HttpError(404, 'This word not found');
  }

  if (foundWord.owner.toString() !== ownerId.toString()) {
    throw HttpError(403, "You don't have right to edit this word");
  }
  const updateWord = {
    ...req.body,
    isEnSpelling: false,
    isUaSpelling: false,
    ...(foundWord.category === 'verb' && req.body.category !== 'verb' && { $unset: { isIrregular: 1 } }),
  };

  const result = await Word.findByIdAndUpdate(wordId, updateWord, { new: true }).select('-createdAt -updatedAt');
  const { isEnSpelling, isUaSpelling, ...handledResult } = result.toObject();

  let progress;
  if (isEnSpelling && isUaSpelling) {
    progress = 100;
  } else if (isEnSpelling || isUaSpelling) {
    progress = 50;
  } else {
    progress = 0;
  }

  res.json({ ...handledResult, progress });
};

module.exports = editWord;
