const { CtrlWrapper } = require('../../helpers');
const getCategories = require('./getCategories');
const createWord = require('./createWord');
const getOwnWords = require('./getOwnWords');
const getAllWords = require('./getAllWords');
const addWord = require('./addWord');
const editWord = require('./editWord');
const removeWord = require('./removeWord');
const getStatistics = require('./getStatistics');
const getTasks = require('./getTasks');
const postAnswers = require('./postAnswers');

module.exports = {
  getCategories: CtrlWrapper(getCategories),
  createWord: CtrlWrapper(createWord),
  addWord: CtrlWrapper(addWord),
  getOwnWords: CtrlWrapper(getOwnWords),
  getAllWords: CtrlWrapper(getAllWords),
  editWord: CtrlWrapper(editWord),
  removeWord: CtrlWrapper(removeWord),
  getStatistics: CtrlWrapper(getStatistics),
  getTasks: CtrlWrapper(getTasks),
  postAnswers: CtrlWrapper(postAnswers),
};
