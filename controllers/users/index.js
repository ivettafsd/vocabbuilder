const {  CtrlWrapper } = require('../../helpers');
const signup = require('./signup');
const signin = require('./signin');
const currentUser = require('./currentUser');
const signout = require('./signout');

module.exports = {
  signup: CtrlWrapper(signup),
  signin: CtrlWrapper(signin),
  currentUser: CtrlWrapper(currentUser),
  signout: CtrlWrapper(signout),
};
