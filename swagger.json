{
  "openapi": "3.0.1",
  "info": {
    "version": "1.0.0",
    "title": "VocabBuilder",
    "description": "VocabBuilder backend"
  },
  "consumes": ["application/json"],
  "produces": ["application/json"],
  "servers": [{ "url": "https://vocabbuilder.onrender.com/api" }],
  "tags": [
    {
      "name": "Auth",
      "description": "Authorization endpoints"
    },
    {
      "name": "Word",
      "description": "Word endpoints"
    }
  ],
  "paths": {
    "/users/signup": {
      "post": {
        "tags": ["Auth"],
        "summary": "User sign up",
        "requestBody": {
          "description": "Sign up object",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/SignUpRequest"
              }
            }
          }
        },
        "responses": {
          "201": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SignUpResponse"
                }
              }
            }
          },
          "400": {
            "description": "Bad request (invalid request body)",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "409": {
            "description": "Such email already exists",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/users/signin": {
      "post": {
        "tags": ["Auth"],
        "summary": "User sign in",
        "requestBody": {
          "description": "Sign in object",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/SignInRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SignInResponse"
                }
              }
            }
          },
          "400": {
            "description": "Bad request (invalid request body)",
            "content": {}
          },
          "401": {
            "description": "Email or password invalid",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/users/current": {
      "get": {
        "tags": ["Auth"],
        "summary": "Get current user info",
        "security": [
          {
            "Bearer": []
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GetCurrentResponse"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/users/signout": {
      "post": {
        "tags": ["Auth"],
        "summary": "User sign out",
        "security": [
          {
            "Bearer": []
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/SignOutResponse"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/categories": {
      "get": {
        "tags": ["Word"],
        "summary": "Get word's categories",
        "security": [
          {
            "Bearer": []
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GetCategoriesResponse"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/create": {
      "post": {
        "tags": ["Word"],
        "summary": "Create a new word",
        "security": [
          {
            "Bearer": []
          }
        ],
        "requestBody": {
          "description": "New word object",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/CreateNewWordRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/CreateNewWordResponse"
                }
              }
            }
          },
          "400": {
            "description": "Bad request (invalid request body)",
            "content": {}
          },
          "401": {
            "description": "Such a word exists",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/add/{id}": {
      "post": {
        "tags": ["Word"],
        "summary": "Add a new word from foreign user",
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "description": "Word id",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/AddNewWordResponse"
                }
              }
            }
          },
          "400": {
            "description": "Bad request (invalid request body)",
            "content": {}
          },
          "401": {
            "description": "Such a word exists",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/edit/{id}": {
      "patch": {
        "tags": ["Word"],
        "summary": "Edit word",
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "description": "Word id",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "requestBody": {
          "description": "Word object",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/EditWordRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/EditWordResponse"
                }
              }
            }
          },
          "400": {
            "description": "Bad request (invalid request body)",
            "content": {}
          },
          "401": {
            "description": "This word not found",
            "content": {}
          },
          "403": {
            "description": "You don't have right to edit this word",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/all": {
      "get": {
        "tags": ["Word"],
        "summary": "Get all words of other users",
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "in": "query",
            "name": "keyword",
            "description": "The keyword for searching",
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "query",
            "name": "category",
            "description": "The category's name. Allowed values are 'verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase'",
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "query",
            "name": "isIrregular",
            "description": "If the category is a verb, it's irregular verb or not?",
            "schema": {
              "type": "boolean"
            }
          },
          {
            "in": "query",
            "name": "page",
            "description": "The number page of the words",
            "schema": {
              "type": "number",
              "default": 1
            }
          },
          {
            "in": "query",
            "name": "limit",
            "description": "The limit for the words",
            "schema": {
              "type": "number",
              "default": 7
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GetAllWordsResponse"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/own": {
      "get": {
        "tags": ["Word"],
        "summary": "Get User's words",
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "in": "query",
            "name": "keyword",
            "description": "The keyword for searching",
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "query",
            "name": "category",
            "description": "The category's name. Allowed values are 'verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase'",
            "schema": {
              "type": "string"
            }
          },
          {
            "in": "query",
            "name": "isIrregular",
            "description": "If the category is a verb, it's irregular verb or not?",
            "schema": {
              "type": "boolean"
            }
          },
          {
            "in": "query",
            "name": "page",
            "description": "The number page of the words",
            "schema": {
              "type": "number",
              "default": 1
            }
          },
          {
            "in": "query",
            "name": "limit",
            "description": "The limit for the words",
            "schema": {
              "type": "number",
              "default": 7
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GetUsersWordsResponse"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/delete/{id}": {
      "delete": {
        "tags": ["Word"],
        "summary": "Delete User's word",
        "security": [
          {
            "Bearer": []
          }
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "description": "Word id",
            "schema": {
              "type": "string"
            },
            "required": true
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/DeleteWordResponse"
                }
              }
            }
          },
          "400": {
            "description": "Bad request (invalid request body)",
            "content": {}
          },
          "401": {
            "description": "This word not found",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/statistics": {
      "get": {
        "tags": ["Word"],
        "summary": "Get User's statistics",
        "security": [
          {
            "Bearer": []
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GetUsersStatisticsResponse"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/tasks": {
      "get": {
        "tags": ["Word"],
        "summary": "Get User's tasks",
        "security": [
          {
            "Bearer": []
          }
        ],
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/GetUsersTasksResponse"
                }
              }
            }
          },
          "401": {
            "description": "Unauthorized",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    },
    "/words/answers": {
      "post": {
        "tags": ["Word"],
        "summary": "Post answers",
        "security": [
          {
            "Bearer": []
          }
        ],
        "requestBody": {
          "description": "Answers object",
          "required": true,
          "content": {
            "application/json": {
              "schema": {
                "$ref": "#/components/schemas/PostAnswersRequest"
              }
            }
          }
        },
        "responses": {
          "200": {
            "description": "Successful operation",
            "content": {
              "application/json": {
                "schema": {
                  "$ref": "#/components/schemas/PostAnswersResponse"
                }
              }
            }
          },
          "400": {
            "description": "Bad request (invalid request body)",
            "content": {}
          },
          "404": {
            "description": "Service not found",
            "content": {}
          },
          "500": {
            "description": "Server error",
            "content": {}
          }
        }
      }
    }
  },
  "components": {
    "schemas": {
      "SignUpRequest": {
        "type": "object",
        "required": ["name", "email", "password"],
        "properties": {
          "name": {
            "type": "string",
            "description": "User name as string",
            "example": "TestName"
          },
          "email": {
            "type": "string",
            "description": "User email as a string with a valid email address format",
            "example": "test@gmail.com"
          },
          "password": {
            "type": "string",
            "description": "User password as a string which consists of 6 English letters and 1 number",
            "example": "testtt1"
          }
        }
      },
      "SignUpResponse": {
        "type": "object",
        "properties": {
          "email": {
            "type": "string",
            "description": "User email",
            "example": "test@gmail.com"
          },
          "name": {
            "type": "string",
            "description": "User name",
            "example": "TestName"
          },
          "token": {
            "type": "string",
            "description": "User token",
            "example": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YjA3MDAzOTJhMjMxNWFiODk2MGMxYiIsImlhdCI6MTY4OTI4NDYxMiwiZXhwIjoxNjg5MzY3NDEyfQ.a8GLmpGRC0BWzhLyd2UVTpH0Cp6j-m9hrxu4MCnaQPM"
          }
        },
        "example": {
          "email": "test@gmail.com",
          "name": "TestName",
          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YjA3MDAzOTJhMjMxNWFiODk2MGMxYiIsImlhdCI6MTY4OTI4NDYxMiwiZXhwIjoxNjg5MzY3NDEyfQ.a8GLmpGRC0BWzhLyd2UVTpH0Cp6j-m9hrxu4MCnaQPM"
        }
      },
      "SignInRequest": {
        "type": "object",
        "required": ["email", "password"],
        "properties": {
          "email": {
            "type": "string",
            "description": "User email as a string with a valid email address format",
            "example": "test@gmail.com"
          },
          "password": {
            "type": "string",
            "description": "User password as a string which consists of 6 English letters and 1 number",
            "example": "testtt1"
          }
        }
      },
      "SignInResponse": {
        "type": "object",
        "properties": {
          "email": {
            "type": "string",
            "description": "User email",
            "example": "test@gmail.com"
          },
          "name": {
            "type": "string",
            "description": "User name",
            "example": "TestName"
          },
          "token": {
            "type": "string",
            "description": "User token",
            "example": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YjA3MDAzOTJhMjMxNWFiODk2MGMxYiIsImlhdCI6MTY4OTI4NDYxMiwiZXhwIjoxNjg5MzY3NDEyfQ.a8GLmpGRC0BWzhLyd2UVTpH0Cp6j-m9hrxu4MCnaQPM"
          }
        },
        "example": {
          "email": "test@gmail.com",
          "name": "TestName",
          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YjA3MDAzOTJhMjMxNWFiODk2MGMxYiIsImlhdCI6MTY4OTI4Nzg0OCwiZXhwIjoxNjg5MzcwNjQ4fQ.LoDGkboiEfTDoAvt6edVXGdKErdcaby5RJ6n5HS5VHg"
        }
      },
      "GetCurrentResponse": {
        "type": "object",
        "properties": {
          "_id": {
            "type": "string",
            "description": "User id",
            "example": "64b1e25f6b0a2ccb95591ec7"
          },
          "name": {
            "type": "string",
            "description": "User name",
            "example": "TestName"
          },
          "email": {
            "type": "string",
            "description": "User email",
            "example": "test@gmail.com"
          },
          "token": {
            "type": "string",
            "description": "User token",
            "example": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YjFlMjVmNmIwYTJjY2I5NTU5MWVjNyIsImlhdCI6MTY4OTM3OTQyMywiZXhwIjoxNjg5NDYyMjIzfQ.hT2Ta6pBhDR1vOF7LjcKxofyASDPjcTZtFi9CESKIuA"
          }
        },
        "example": {
          "_id": "64b1e25f6b0a2ccb95591ec7",
          "name": "TestName",
          "email": "test@gmail.com",
          "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YjFlMjVmNmIwYTJjY2I5NTU5MWVjNyIsImlhdCI6MTY4OTM3OTQyMywiZXhwIjoxNjg5NDYyMjIzfQ.hT2Ta6pBhDR1vOF7LjcKxofyASDPjcTZtFi9CESKIuA"
        }
      },
      "SignOutResponse": {
        "type": "object",
        "properties": {
          "message": {
            "type": "string",
            "description": "Sign out success",
            "example": "Sign out success"
          }
        },
        "example": {
          "message": "Sign out success"
        }
      },
      "GetCategoriesResponse": {
        "type": "array",
        "items": {
          "type": "string"
        },
        "example": [
          "verb",
          "participle",
          "noun",
          "adjective",
          "pronoun",
          "numerals",
          "adverb",
          "preposition",
          "conjunction",
          "phrasal verb",
          "functional phrase"
        ]
      },
      "CreateNewWordRequest": {
        "type": "object",
        "required": ["en", "ua", "category"],
        "properties": {
          "en": {
            "type": "string",
            "description": "New word in English",
            "example": "know-knew-known"
          },
          "ua": {
            "type": "string",
            "description": "New word in Ukrainian",
            "example": "знати"
          },
          "category": {
            "type": "string",
            "description": "Allowed values are 'verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase'",
            "example": "verb"
          },
          "isIrregular": {
            "type": "boolean",
            "description": "The isIrregular field is required if category is verb",
            "example": "true"
          }
        }
      },
      "CreateNewWordResponse": {
        "type": "object",
        "properties": {
          "_id": {
            "type": "string",
            "description": "New word id",
            "example": "64c2e7c8a7c8fe217d5b00bc"
          },
          "en": {
            "type": "string",
            "description": "New word in English",
            "example": "TestName"
          },
          "ua": {
            "type": "string",
            "description": "New word in Ukrainian",
            "example": "знати"
          },
          "category": {
            "type": "string",
            "description": "New word's category",
            "example": "verb"
          },
          "isIrregular": {
            "type": "boolean",
            "description": "Verb's type",
            "example": true
          },
          "owner": {
            "type": "string",
            "description": "New word's owner",
            "example": "64c2e73f8966d2fef3ac56d1"
          },
          "progress": {
            "type": "number",
            "description": "Owner's progress regarding new word",
            "example": 0
          }
        },
        "example": {
          "_id": "64c2d5652afb66061c3bd0ac",
          "en": "know-knew-known",
          "ua": "знати",
          "category": "verb",
          "isIrregular": true,
          "owner": "64bfc0d7cd20e3eff55ae6d7",
          "progress": 0
        }
      },
      "AddNewWordResponse": {
        "type": "object",
        "properties": {
          "_id": {
            "type": "string",
            "description": "New word id",
            "example": "64c2e7c8a7c8fe217d5b00bc"
          },
          "en": {
            "type": "string",
            "description": "New word in English",
            "example": "TestName"
          },
          "ua": {
            "type": "string",
            "description": "New word in Ukrainian",
            "example": "знати"
          },
          "category": {
            "type": "string",
            "description": "New word's category",
            "example": "verb"
          },
          "isIrregular": {
            "type": "boolean",
            "description": "Verb's type",
            "example": true
          },
          "owner": {
            "type": "string",
            "description": "New word's owner",
            "example": "64c2e73f8966d2fef3ac56d1"
          },
          "progress": {
            "type": "number",
            "description": "Owner's progress regarding new word",
            "example": 0
          }
        },
        "example": {
          "_id": "64c2d5652afb66061c3bd0ac",
          "en": "know-knew-known",
          "ua": "знати",
          "category": "verb",
          "isIrregular": true,
          "owner": "64bfc0d7cd20e3eff55ae6d7",
          "progress": 0
        }
      },
      "EditWordRequest": {
        "type": "object",
        "required": ["en", "ua", "category"],
        "properties": {
          "en": {
            "type": "string",
            "description": "New word's option in English",
            "example": "know-knew-known"
          },
          "ua": {
            "type": "string",
            "description": "New word's option in Ukrainian",
            "example": "знати"
          },
          "category": {
            "type": "string",
            "description": "Allowed values are 'verb', 'participle', 'noun', 'adjective', 'pronoun', 'numerals', 'adverb', 'preposition', 'conjunction', 'phrasal verb', 'functional phrase'",
            "example": "verb"
          },
          "isIrregular": {
            "type": "boolean",
            "description": "The isIrregular field is required if category is verb",
            "example": "true"
          }
        }
      },
      "EditWordResponse": {
        "type": "object",
        "properties": {
          "_id": {
            "type": "string",
            "description": "Word id",
            "example": "64c2e7c8a7c8fe217d5b00bc"
          },
          "en": {
            "type": "string",
            "description": "New word's option in English",
            "example": "know-knew-known"
          },
          "ua": {
            "type": "string",
            "description": "New word's option in Ukrainian",
            "example": "знати"
          },
          "category": {
            "type": "string",
            "description": "New word's category",
            "example": "verb"
          },
          "isIrregular": {
            "type": "boolean",
            "description": "Verb's type",
            "example": true
          },
          "owner": {
            "type": "string",
            "description": "New word's owner",
            "example": "64c2e73f8966d2fef3ac56d1"
          },
          "progress": {
            "type": "number",
            "description": "Owner's progress regarding word",
            "example": 0
          }
        },
        "example": {
          "_id": "64c440d5bf4bb7c74ebbdf03",
          "en": "know-knew-known",
          "ua": "знати",
          "category": "verb",
          "isIrregular": true,
          "owner": "64bfc0d7cd20e3eff55ae6d7",
          "progress": 0
        }
      },
      "GetAllWordsResponse": {
        "type": "object",
        "properties": {
          "results": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "_id": {
                  "type": "string",
                  "description": "Word id",
                  "example": "64c2e7c8a7c8fe217d5b00bc"
                },
                "en": {
                  "type": "string",
                  "description": "Word in English",
                  "example": "know-knew-known"
                },
                "ua": {
                  "type": "string",
                  "description": "Word in Ukrainian",
                  "example": "знати"
                },
                "category": {
                  "type": "string",
                  "description": "Word's category",
                  "example": "verb"
                },
                "isIrregular": {
                  "type": "boolean",
                  "description": "Verb's type",
                  "example": true
                }
              }
            }
          },
          "totalPages": {
            "type": "number"
          },
          "page": {
            "type": "number"
          },
          "perPage": {
            "type": "number"
          }
        },
        "example": {
          "results": [
            {
              "_id": "64c2ec0f6f9cf4775f2324c7",
              "en": "know-knew-known",
              "ua": "знати",
              "category": "verb",
              "isIrregular": true
            }
          ],
          "totalPages": 2,
          "page": 2,
          "perPage": 2
        }
      },
      "GetUsersWordsResponse": {
        "type": "object",
        "properties": {
          "results": {
            "type": "array",
            "items": {
              "type": "object",
              "properties": {
                "_id": {
                  "type": "string",
                  "description": "Word id",
                  "example": "64c2e7c8a7c8fe217d5b00bc"
                },
                "en": {
                  "type": "string",
                  "description": "Word in English",
                  "example": "know-knew-known"
                },
                "ua": {
                  "type": "string",
                  "description": "Word in Ukrainian",
                  "example": "знати"
                },
                "category": {
                  "type": "string",
                  "description": "Word's category",
                  "example": "verb"
                },
                "isIrregular": {
                  "type": "boolean",
                  "description": "Verb's type",
                  "example": true
                },
                "owner": {
                  "type": "string",
                  "description": "User id",
                  "example": "64c6dde64b0c8534d41f9b5c"
                },
                "progress": {
                  "type": "number",
                  "description": "User's progress",
                  "example": 50
                }
              }
            }
          },
          "totalPages": {
            "type": "number"
          },
          "page": {
            "type": "number"
          },
          "perPage": {
            "type": "number"
          }
        },
        "example": {
          "results": [
            {
              "_id": "64c6e8d7abbd3d21328a00cf",
              "en": "run-ran-run",
              "ua": "бігти",
              "category": "verb",
              "isIrregular": true,
              "owner": "64c6dde64b0c8534d41f9b5c",
              "progress": 50
            },
            {
              "_id": "64c6e8ecabbd3d21328a00d4",
              "en": "cat",
              "ua": "кіт",
              "category": "noun",
              "owner": "64c6dde64b0c8534d41f9b5c",
              "progress": 50
            },
            {
              "_id": "64c6e8f4abbd3d21328a00d9",
              "en": "dog",
              "ua": "пес",
              "category": "noun",
              "owner": "64c6dde64b0c8534d41f9b5c",
              "progress": 0
            }
          ],
          "totalPages": 1,
          "page": 1,
          "perPage": 7
        }
      },
      "DeleteWordResponse": {
        "type": "object",
        "properties": {
          "_id": {
            "type": "string",
            "description": "New word id",
            "example": "64c2e7c8a7c8fe217d5b00bc"
          },
          "message": {
            "type": "string",
            "description": "Success message",
            "example": "This word was deleted"
          }
        },
        "example": {
          "message": "This word was deleted",
          "id": "64c2e7c8a7c8fe217d5b00bc"
        }
      },
      "GetUsersStatisticsResponse": {
        "type": "object",
        "properties": {
          "totalCount": {
            "type": "number",
            "description": "Amount User's words for training",
            "example": 3
          }
        },
        "example": {
          "totalCount": 3
        }
      },
      "GetUsersTasksResponse": {
        "type": "object",
        "properties": {
          "words": {
            "type": "array",
            "items": {
              "type": "object",
              "_id": {
                "type": "string",
                "description": "Word id",
                "example": "64c44e7b9307a6e92f3a25c3"
              },
              "en": {
                "type": "string",
                "description": "User's word in English",
                "example": "know-knew-known"
              },
              "task": {
                "type": "string",
                "description": "Task for user",
                "example": "ua"
              }
            }
          }
        },
        "example": {
          "words": [
            {
              "_id": "64c44e7b9307a6e92f3a25c3",
              "ua": "знати",
              "task": "en"
            },
            {
              "_id": "64c44ea29307a6e92f3a25c8",
              "ua": "кіт",
              "task": "en"
            },
            {
              "_id": "64c44eb59307a6e92f3a25cd",
              "ua": "пес",
              "task": "en"
            }
          ]
        }
      },
      "PostAnswersRequest": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "_id": {
              "type": "string",
              "description": "Word id",
              "example": "64c2d54d2afb66061c3bd0a2"
            },
            "en": {
              "type": "string",
              "description": "Word in English",
              "example": "know-knew-known"
            },
            "ua": {
              "type": "string",
              "description": "Word in Ukrainian",
              "example": "знати"
            },
            "task": {
              "type": "string",
              "description": "Allowed values are 'en', 'ua'",
              "example": "en"
            }
          }
        }
      },
      "PostAnswersResponse": {
        "type": "array",
        "items": {
          "type": "object",
          "properties": {
            "_id": {
              "type": "string",
              "description": "Word id",
              "example": "64c2d54d2afb66061c3bd0a2"
            },
            "en": {
              "type": "string",
              "description": "Word in English",
              "example": "know-knew-known"
            },
            "ua": {
              "type": "string",
              "description": "Word in Ukrainian",
              "example": "знати"
            },
            "task": {
              "type": "string",
              "description": "Allowed values are 'en', 'ua'",
              "example": "en"
            },
            "isDone": {
              "type": "boolean",
              "description": "Answer's result",
              "example": true
            }
          }
        },
        "example": [
          {
            "_id": "64c2ec0f6f9cf4775f2324c7",
            "ua": "знати",
            "task": "en",
            "en": "know-knew-known",
            "isDone": true
          },
          {
            "_id": "64c386bf90a139eb57ad1d64",
            "ua": "ноутбук",
            "task": "en",
            "en": "laptop",
            "isDone": false
          }
        ]
      }
    },
    "securitySchemes": {
      "Bearer": {
        "type": "http",
        "scheme": "bearer",
        "bearerFormat": "JWT"
      }
    }
  }
}
